// Add the input field
$(document).ready(function () {
    $('.customerInfoContainer').after($('<div style="position: relative; right: 0; top: 0; background-color: #FFF; padding: 5px; border: 1px solid #000; width: 25%; height: 68%; font-size: small; margin-left: 5px; margin-right: 5px; margin-top: 5px; float:left" id="customerAgeDiv">Age of Customer: <input type="number" id="customerAge"></div>'));
});
// monkey path the original execute method.
var origMethod = BasicReceiptActionCommand.prototype.execute;


// add our own code and call the original method we monkey patched.
BasicReceiptActionCommand.prototype.execute = function (a, b, c) {
    if (a.entity === "createReceipt") {
        var receiptKey = $('#receiptEntityKey').val();
        var requestBody = '{' +  "\"customerAge\" : " + $("#customerAge").val() + ' }';
        $.ajax({
            type: "POST",
            url: "PluginServlet?action=customerAge&receiptId="+encodeURIComponent(receiptKey),
            data: requestBody,
            contentType: "application/json",
            dataType: "json",
            success: function(result) {
                if (result && result["status"] == 1) {
                    $("#customerAge").val(0);
                }
            }
        });
    }
    origMethod.call(this, a, b, c);
};
