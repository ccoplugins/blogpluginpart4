package com.be1eye.cco.blogpluginpart4;

import com.be1eye.cco.blogpluginpart4.dao.CustomerAgeDao;
import com.be1eye.cco.blogpluginpart4.dto.CustomerAgeDto;
import com.sap.scco.util.logging.Logger;
import net.sf.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * @author RobertZieschang
 */
public class RequestDelegate {


    private static final Logger logger = Logger.getLogger(RequestDelegate.class);

    private CustomerAgeDao customerAgeDao;

    private static RequestDelegate instance = new RequestDelegate();
    public static synchronized RequestDelegate getInstance() {
        if (instance == null) {
            instance = new RequestDelegate();
        }
        return instance;
    }

    private RequestDelegate() {
        this.customerAgeDao = new CustomerAgeDao();
    }

    public void handleRequest(HttpServletRequest request, HttpServletResponse response) {
        String urlMethod = request.getMethod();
        String action = request.getParameter("action");
        JSONObject responseObj = new JSONObject();

        if ("POST".equals(urlMethod)) {
            JSONObject requestBody = this.getRequestBody(request);
            switch(action) {
                case "customerAge":
                    String receiptKey = request.getParameter("receiptId");
                    customerAgeDao.save(new CustomerAgeDto(receiptKey, requestBody.getInt("customerAge")));
                    responseObj.put("status", 1);
                    break;
            }
        } else if ("GET".equals(urlMethod)) {

        }

        if(!responseObj.isEmpty()) {
            try (OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream())) {
                osw.write(responseObj.toString());
            } catch (IOException e) {
                logger.severe("Error while Processing Request");
            }
        }
    }

    private JSONObject getRequestBody(HttpServletRequest request) {

        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = request.getReader();
            String line = null;

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            logger.severe("Error while parsing JSON Request body");
        }
        return JSONObject.fromObject(sb.toString());
    }
}
