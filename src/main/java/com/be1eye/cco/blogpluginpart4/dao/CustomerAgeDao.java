package com.be1eye.cco.blogpluginpart4.dao;


import com.be1eye.cco.blogpluginpart4.dto.CustomerAgeDto;
import com.sap.scco.ap.pos.dao.CDBSession;
import com.sap.scco.ap.pos.dao.CDBSessionFactory;
import com.sap.scco.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author RobertZieschang
 */
public class CustomerAgeDao {

    private static final Logger logger = Logger.getLogger(CustomerAgeDao.class);

    private static final String TABLE_NAME = "HOK_CUSTOMERAGE";

    private static final String QUERY_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
            + "RECEIPTKEY varchar(100) not null PRIMARY KEY,"
            + "CUSTOMERAGE INT not null)";

    private static final String QUERY_INSERT_ROW = "INSERT INTO " + TABLE_NAME + " VALUES(?,?)";

    private static final String QUERY_UPDATE_ROW = "UPDATE " + TABLE_NAME
            + " SET RECEIPTKEY = ?2 WHERE CUSTOMERAGE = ?1";


    // query for find all
    private static final String QUERY_FIND_ALL = "SELECT RECEIPTKEY, CUSTOMERAGE FROM " + TABLE_NAME;

    // query for find one
    private static final String QUERY_FIND_ONE = "SELECT RECEIPTKEY, CUSTOMERAGE FROM " + TABLE_NAME
            + " where RECEIPTKEY = ?";

    // query to drop one
    private static final String QUERY_DROP_ONE = "DELETE FROM " + TABLE_NAME + " WHERE RECEIPTKEY = ?";


    public void setupTable() {
        CDBSession session = CDBSessionFactory.instance.createSession();
        try {
            session.beginTransaction();
            EntityManager em = session.getEM();

            Query q = em.createNativeQuery(QUERY_CREATE_TABLE);
            q.executeUpdate();

            session.commitTransaction();
            logger.info("Created table " + TABLE_NAME);

        } catch (Exception e) {
            session.rollbackDBSession();
            logger.info("Error or table " + TABLE_NAME + " already existing");
        } finally {
            session.closeDBSession();
        }
    }

    private void save(CustomerAgeDto customerAge, boolean isAlreadyInDB) {

        CDBSession session = CDBSessionFactory.instance.createSession();
        String query = isAlreadyInDB ? QUERY_UPDATE_ROW : QUERY_INSERT_ROW;

        try {
            session.beginTransaction();
            EntityManager em = session.getEM();

            Query q = em.createNativeQuery(query);
            q.setParameter(1, customerAge.getReceiptKey());
            q.setParameter(2, customerAge.getCustomerAge());

            q.executeUpdate();
            session.commitTransaction();

        } catch (Exception e) {
            session.rollbackDBSession();
            logger.info("Could not create CustomerAge");
            logger.info(e.getLocalizedMessage());
        } finally {
            session.closeDBSession();
        }
    }

    public List<CustomerAgeDto> findAll() {
        CDBSession session = CDBSessionFactory.instance.createSession();
        ArrayList<CustomerAgeDto> resultList = new ArrayList<>();

        try {
            session.beginTransaction();
            EntityManager em = session.getEM();

            Query q = em.createNativeQuery(QUERY_FIND_ALL);
            @SuppressWarnings("unchecked")
            List<Object[]> results = q.getResultList();

            if (results.isEmpty()) {
                // return an empty list rather than null
                return resultList;
            }

            for (Object[] resultRow : results) {
                resultList.add(new CustomerAgeDto((String) resultRow[0], (Integer) resultRow[1]));
            }

        } catch (Exception e) {
            logger.info("Error while getting results from table " + TABLE_NAME);

        } finally {
            session.closeDBSession();

        }

        return resultList;
    }

    public CustomerAgeDto findOne(String inReceiptKey) {
        CDBSession session = CDBSessionFactory.instance.createSession();
        CustomerAgeDto customerAge = new CustomerAgeDto();

        try {

            EntityManager em = session.getEM();
            Query q = em.createNativeQuery(QUERY_FIND_ONE);
            q.setParameter(1, inReceiptKey);

            @SuppressWarnings("unchecked")
            List<Object[]> results = q.getResultList();

            if (results.isEmpty()) {
                // return empty dto
                return customerAge;
            }

            customerAge.setReceiptKey((String) results.get(0)[0]);
            customerAge.setCustomerAge((Integer) results.get(0)[1]);

        } catch (Exception e) {
            logger.info("Error while getting " + inReceiptKey + " from table " + TABLE_NAME);

        } finally {
            session.closeDBSession();
        }

        return customerAge;
    }


    public void dropOne(String inReceiptKey) {
        CDBSession session = CDBSessionFactory.instance.createSession();

        try {
            session.beginTransaction();
            EntityManager em = session.getEM();
            Query q = em.createNativeQuery(QUERY_DROP_ONE);
            q.setParameter(1, inReceiptKey);
            q.executeUpdate();
            session.commitTransaction();

        } catch (Exception ex) {

        } finally {
            session.closeDBSession();
        }
    }


    public void dropAll() {
        List<CustomerAgeDto> list = this.findAll();

        for (CustomerAgeDto entry : list) {
            this.dropOne(entry.getReceiptKey());
        }
    }


    public void save(CustomerAgeDto customerAge) {
        CustomerAgeDto customerAgeInDb = this.findOne(customerAge.getReceiptKey());
        boolean isAlreadyInDb = customerAge.getReceiptKey().equals(customerAgeInDb.getReceiptKey());
        // check if entity is already in database, so that we update rather than insert.
        this.save(customerAge, isAlreadyInDb);

    }
}
