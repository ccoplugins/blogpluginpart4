package com.be1eye.cco.blogpluginpart4;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.be1eye.cco.blogpluginpart4.dao.CustomerAgeDao;
import com.be1eye.cco.blogpluginpart4.dto.CustomerAgeDto;
import com.sap.scco.ap.plugin.annotation.ListenToExit;
import com.sap.scco.ap.plugin.annotation.ui.JSInject;
import com.sap.scco.ap.plugin.BasePlugin;
import com.sap.scco.ap.pos.dto.ReceiptDTO;
import com.sap.scco.util.logging.Logger;
import generated.GenericValues;
import generated.PostInvoiceType;


/**
 * @author RobertZieschang zieschang@be1eye.de
 *
 */
public class App extends BasePlugin {
	
	private static final Logger logger = Logger.getLogger(App.class);

	private CustomerAgeDao customerAgeDao;

	@Override
	public String getId() {
		return "pluginpartfour";
	}

	@Override
	public String getName() {
		return "CCO Plugin Part 4";
	}

	@Override
	public String getVersion() {
		return getClass().getPackage().getImplementationVersion();
	}

	@Override 
	public void startup() {

		customerAgeDao = new CustomerAgeDao();
		customerAgeDao.setupTable();
		
		super.startup();

	}
	

	@JSInject(targetScreen = "sales")
	public InputStream[] injectJS() {
		return new InputStream[] {this.getClass().getResourceAsStream("/resources/salesInject.js")};
	}
	
	@ListenToExit(exitName="PluginServlet.callback.post")
	public void pluginServletPost(Object caller, Object[] args) {
		
		HttpServletRequest request = (HttpServletRequest)args[0];
		HttpServletResponse response = (HttpServletResponse)args[1];

		RequestDelegate.getInstance().handleRequest(request, response);
	}

	@ListenToExit(exitName="BusinessOneServiceWrapper.beforePostInvoiceRequest")
	public void enlargeB1iMessage(Object caller, Object[] args) {

		ReceiptDTO receiptDto = (ReceiptDTO) args[0];
		PostInvoiceType request = (PostInvoiceType) args[1];
		CustomerAgeDto customerAge = this.customerAgeDao.findOne(receiptDto.getKey());
		if (null != customerAge.getReceiptKey()) {
			GenericValues.KeyValPair keyValPair = new GenericValues.KeyValPair();
			keyValPair.setKey("customerAge");
			keyValPair.setValue(customerAge.getCustomerAge().toString());

			if (null == request.getGenericValues()) {
				request.getSale().getDocuments().setGenericValues(new GenericValues());
			}

            request.getSale().getDocuments().getGenericValues().getKeyValPair().add(keyValPair);
			args[1] = request;
		}
	}
}
