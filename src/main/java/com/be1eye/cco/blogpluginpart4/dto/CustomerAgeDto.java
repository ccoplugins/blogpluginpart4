package com.be1eye.cco.blogpluginpart4.dto;

/**
 * @author RobertZieschang
 */
public class CustomerAgeDto {

    private String receiptKey;

    private Integer customerAge;

    public CustomerAgeDto() {

    }

    public CustomerAgeDto(String inReceipKey, Integer inCustomerAge) {
        this.receiptKey = inReceipKey;
        this.customerAge = inCustomerAge;
    }

    public String getReceiptKey() {
        return receiptKey;
    }

    public void setReceiptKey(String receiptKey) {
        this.receiptKey = receiptKey;
    }

    public Integer getCustomerAge() {
        return customerAge;
    }

    public void setCustomerAge(Integer customerAge) {
        this.customerAge = customerAge;
    }
}
